# Déclaration de Confidentialité

L'application CERHIS est une Application développée par AEDES. Elle ne peut être utilisée qu'avec l'accord d'AEDES.

Cette page sert à informer les utilisateurs de l'application CERHIS sur comment sont gérées les données (collecte, utilisation et partage des Informations Personnelles) de toutes personnes utilisant notre Application.

Si vous décidez d'utiliser notre Application, vous acceptez la collecte et l'utilisation de vos données personnelles comme indiqué dans cette Déclaration de Confidentialité. Les Informations Personnelles que nous collectons sont utilisées pour le fonctionnement et l'amélioration de l’Application. Nous n'utiliserons ou ne partagerons pas vos informations avec des personnes tierces, à l'exception de ce qui est décrit dans cette Déclaration de Confidentialité.
## Collecte d'informations et utilisation
Pour une meilleure expérience lors de l'utilisation de notre Application, nous pouvons vous demander certaines informations personnelles, telles que votre nom d'utilisateur ou votre nom. Les informations que nous demandons seront utilisées et partagées comme décrit dans cette Déclaration de Confidentialité.

### Identification unique de l’appareil
Cette Application peut suivre l’Utilisateur en enregistrant un identifiant unique de son appareil à des fins d’analyse ou pour enregistrer ses préférences. L'Application utilise des services tiers qui pourraient récolter des informations permettant de vous identifier.

### Services
Nous pouvons faire appel à des sociétés tierces et à de tierces personnes pour faciliter la prestation de notre Application ("Services Tiers"), assurer le service de l'Application en notre nom, assurer des services liés à l'Application ou nous aider à analyser la façon dont notre Application est utilisée. Ces Services Tiers sont notamment (liste non exhaustive) :
- **Crashlytics** : différents types de Données indiquées dans la politique de confidentialité de l'Application et position géographique 
- **Firebase Crash Reporting** : différents types de Données indiquées dans la politique de confidentialité de l'Application

## Autorisations du dispositif pour accéder aux Données personnelles
Selon le dispositif particulier de l'Utilisateur, cette Application pourrait demander certaines autorisations pour lui autoriser l'accès aux Données du dispositif de l'Utilisateur comme décrit ci-dessous.

Par défaut, ces autorisations doivent être accordées par l’Utilisateur avant que les informations respectives soient accessibles. Une fois que l’autorisation a été donnée, elle peut être révoquée par l’Utilisateur à tout moment. Afin de révoquer ces autorisations, les Utilisateurs peuvent consulter les paramètres du dispositif ou contacter le Propriétaire aux coordonnées fournies dans le présent document. La procédure exacte pour contrôler les permissions des applications peut dépendre du dispositif et du logiciel de l’Utilisateur.

Veuillez noter que la révocation de ces autorisations peut affecter le bon fonctionnement de cette Application.

Si l’Utilisateur accorde l’une des autorisations répertoriées ci-dessous, ces Données Personnelles respectives peuvent être traitées (c’est-à-dire accessibles, modifiées ou supprimées) par cette Application.
### Autorisation de stockage
Utilisée pour accéder à un stockage externe partagé, notamment la lecture et l’ajout d’éléments. Cette autorisation est nécessaire afin de sauvegarder les rapports de données agrégées (fichiers .csv) sur la tablette.
## Log Data
Nous souhaitons vous informer que lorsque vous utilisez notre Application, en cas d'erreur logicielle et de crash dans celle-ci, nous collectons des données et des informations (à travers des librairies tierces) appelées "Log Data". Ces Log Data peuvent inclure des informations comme l'adresse IP de votre appareil, la version du système d'opération (OS), le nom de l'appareil, la configuration de l'Application lors de son utilisation, l'heure et la date de l'utilisation, et d'autres statistiques.

## Sécurité
Votre confiance est importante pour nous, c'est pourquoi nous mettons tout en oeuvre pour protéger vos informations personnelles. Cependant, aucune méthode de transmission par Internet ou méthode de stockage électronique n'est sûre à 100%, et nous ne pouvons donc garantir une sécurité absolue.

## Lien vers d'autres sites
Il se peut que notre Service contienne des liens pointant vers d'autres sites que nous n'exploitons pas. Si vous cliquez sur un lien de tiers, vous serez redirigé vers le site de ce tiers. Nous vous recommandons vivement d'examiner la politique de confidentialité de chacun des sites que vous consultez.
Nous n'avons aucun contrôle sur le contenu, les politiques ou pratiques de confidentialité des sites ou services de tiers et déclinons toute responsabilité en ce qui les concerne.

## Vie privée des enfants
Cette application n'est pas destinée à des personnes de moins de 18 ans ("Enfants"). Nous n'enregistrons pas sciemment d'Informations Personnelles nominatives sur les utilisateurs auprès des personnes de moins de 18 ans. Si vous êtes un parent ou un tuteur et que vous savez que votre Enfant nous a communiqué des Données à Caractère Personnel, veuillez nous contacter. Si nous apprenons que nous avons recueilli des Données à Caractère Personnel auprès d'enfants sans vérifier s'il y a consentement parental, nous ferons le nécessaire pour supprimer ces informations de nos serveurs.

## Modification de cette Déclaration de Confidentialité
Nous nous réservons le droit d'actualiser notre Politique de Confidentialité dans le futur. Nous vous informerons de toute modification en publiant la nouvelle Politique de Confidentialité sur cette page.
Nous vous conseillons de consulter la présente Politique de Confidentialité périodiquement pour prendre connaissance de toute modification. Les modifications apportées à la présente Politique de Confidentialité prennent effet lorsqu'elles sont publiées sur cette page.

## Contact
Si vous avez des questions ou des suggestions sur notre Déclaration de Confidentialité, n'hésitez pas à nous contacter à l'adresse info@cerhis.org

Cette Déclaration de Confidentialité a été créée sur base du template disponible à l'adresse privacypolicytemplate.net.

Cette déclaration de confidentialité a été publiée le 25 septembre 2018.
